<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $fillable = ['name', 'slug'];

  public function  tasks()
  {
    return $this->hasMany('App\Tasks');
  }

}
