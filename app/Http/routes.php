<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::model('projects', 'App\Project');
Route::bind('projects', function($value, $route){
  return App\Project::whereSlug($value)->first();
});

Route::bind('tasks', function($value, $route){
  return App\Tasks::whereSlug($value)->first();
});

Route::get('/', 'WelcomeController@index');

Route::resource('projects', 'ProjectsController');
Route::resource('projects.tasks', 'TasksController');
