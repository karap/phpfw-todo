<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model {

  protected $fillable = ['name', 'slug', 'completed', 'notes', 'project_id'];

  public function  project()
  {
    return $this->hasMany('App\Project');
  }

}
