<?php

use App\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectsTableSeeder extends Seeder {

  public function run()
  {
    DB::table('projects')->delete();

    for($i=1;$i<5;$i++){
      Project::create([
        'name' => "Project $i",
        'slug' => "Project-$i"
      ]);
    }
  }

}