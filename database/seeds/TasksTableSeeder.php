<?php

use App\Tasks;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksTableSeeder extends Seeder {

  public function run()
  {
    DB::table('tasks')->delete();

    for($i=1;$i<10;$i++){
      Tasks::create([
          'name' => "Task $i",
          'slug' => "Task-$i",
        'project_id' => rand(1,4),
        'completed' => false,
        'notes' => "this is task #$i!",
      ]);
    }
  }

}