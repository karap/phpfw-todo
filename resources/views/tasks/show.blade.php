@extends('layouts.main')

@section('content')
    <h2>Project: {{ $project->name }}</h2>
    <a href="{{ route('projects.show', $project->slug) }}">Back to project</a>
    <hr/>
    <h3>{{ $task->name }}</h3>
    <p>
        <strong>Completed:</strong> {{ ($project->completed) ? 'Yes':'No' }}
    </p>
    <p>
        <strong>Notes:</strong>
        {{ $task->notes }}
    </p>
@stop

