@extends('layouts.main')

@section('content')
    <h2>Projects</h2>
    <a href="{{ route('projects.create') }}" class="btn btn-primary btn-sm">Create project</a>
    <hr/>
    @if($projects->count())
        <ul>
            @foreach($projects as $project)
                <li>
                    <a href="{{ route('projects.show', $project->slug) }}">
                        {{ $project->name }}
                    </a>
                    {!! Form::open(['route'=>['projects.destroy', $project->slug], 'method'=>'delete', 'style'=>'display:inline-block;']) !!}
                    {!! Form::submit('delete', ['class'=>'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </li>
            @endforeach
        </ul>
    @else
        <p>
            There were no projects found. <a href="{{ route('projects.create') }}">Add one first.</a>
        </p>
    @endif
@stop

