<div class="form-group">
    {!! Form::label('name') !!}
    {!! Form::text('name', null, ['class'=>'form-control'])  !!}
    {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::label('slug') !!}
    {!! Form::text('slug', null, ['class'=>'form-control']) !!}
    {!! $errors->first('slug', '<p class="text-danger">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($submittext, ['class'=>'btn btn-primary']) !!}
</div>