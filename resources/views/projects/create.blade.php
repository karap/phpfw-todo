@extends('layouts.main')

@section('content')
    <h2>Create new project</h2>
    <a href="{{ route('projects.index') }}">back to projects</a>
    <hr/>
    {!! Form::open(['route'=>'projects.store']) !!}
    @include('projects.form', ['submittext'=>'Create new project'])
    {!! Form::close() !!}
@stop